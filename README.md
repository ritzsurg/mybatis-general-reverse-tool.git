# Mybatis逆向通用工具

#### 介绍
Mybatis逆向通用工具


### 欢迎关注公众号
![输入图片说明](https://images.gitee.com/uploads/images/2020/1220/203948_324b6f0b_8285518.jpeg "个人微信公号.jpg")


或者微信搜索【睿思云动】，即可关注！

 **使用环境** 
目前SpringBoot的发展趋势已经势如破竹，为了更方便的使用，所以此工具暂时只允许在集成了SpringBoot框架的项目中使用。
- SpringBoot1.x以上版本
- JDK 1.6以上版本


### 操作详细过程
点此链接，查看过程： [图解过程](https://blog.csdn.net/qq_38875580/article/details/109878345)

### #使用说明

1.  将压缩包解压，在DOS环境下直接编译，在DOS环境下进入对应目录下操作！
2.  cd到mybatis-generator-core-1.3.2\lib目录下直接运行如下命令：
    在DOS命令行下直接运行：java -jar mybatis-generator-core-1.3.2.jar -configfile generatorConfig.xml -overwrite


通过上述操作即可生成实体，pojo，mapper，简单快捷！






